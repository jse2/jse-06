# 1. Project information

Task manager application

# 2. Software requirements

Java 11, OS Windows

# 3. Technological stack

Java, Maven

# 4. Installation

## 4.1 Build project

```cmd
mvn clean install
```

## 4.2 Run application

```cmd
java -jar task-manager-1.0.6.jar
```

# 5. Contact information

Grigorii Mirsaitov (mirsaitov_gr@nlmk.com)

# 6. Mirror repository

https://github.com/Volan/jse